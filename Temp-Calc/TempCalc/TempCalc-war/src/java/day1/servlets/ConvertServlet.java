/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day1.servlets;

import day1.beans.CalculatorBeanRemote;
import java.io.IOException;
import java.io.PrintWriter;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author ipd11
 */
public class ConvertServlet extends HttpServlet {
    
    //we'll write this here
    //a reference that implements the interface
    @EJB
    //a reference to somethig that we'll use to access the bean (we use the name of the bean remote class)
    CalculatorBeanRemote tempCalcBean;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ConvertServlet</title>");            
            out.println("</head>");
            out.println("<body>");
            
            //adding the form (paste the code inside the empty string)
            
            out.println("<form>\n" +
                        "  <input type=\"text\" name=\"celTemp\">\n" +
                        "  <input type=\"submit\" value=\"Convert\">\n" +
                        "</form>");
            
            //celTemp is the value in the form
            String celStr = request.getParameter("celTemp");
            
            if (celStr != null) {
                
                try {
                    double cel = Double.parseDouble(celStr);
                    //making the call
                    //out.printf("%.2fC in Fahrenheit is %.2F ", cel, tempCalcBean.celToFah(20));
                    out.println("20C in Fahrenheit is: " + tempCalcBean.celToFah(20));
                }
                
                catch (NumberFormatException e){
                    //error message
                    out.println("Error in number format: Reenter the number");
                }                 
            }

            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
