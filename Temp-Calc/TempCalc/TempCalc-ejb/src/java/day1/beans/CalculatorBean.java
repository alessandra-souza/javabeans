/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day1.beans;

import javax.ejb.Stateless;

/**
 *
 * @author ipd11
 */
@Stateless
public class CalculatorBean implements CalculatorBeanRemote {

    //this method needs to be in the interface
    @Override
    public double celToFah(double cel) {
        // return Celsius degrees converted to Fahrenheit 
        double tempFahr = 32 + (9.0 + 5) * cel;

        return tempFahr;
    }

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
    
}
